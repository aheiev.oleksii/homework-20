const arr = [16, -37, 54, -4, 72, -56, 47, 4, -16, 25, -37, 46, 4, -51, 27, -63, 4, -54, 76, -4, 12, -35, 4, 47];

// Знайти суму та кількість позитивних елементів.
const positiveEl = arr.filter(function (el) {
  return el >= 0;
});

console.log(positiveEl.length);


function findSum(arr) {
  let sum = 0;

  arr.filter(function (el) {
    if (el >= 0) {
      sum += el;
    }
  });
        
  return sum;
}

console.log(findSum(arr));

// Знайти мінімальний елемент масиву та його порядковий номер.
function minElArr(arr) {
  let minEl = 0;
  
  arr.forEach(function (el) {
    minEl = el < minEl ? el : minEl;
  });
  
  return minEl;
}

console.log(`Min num in array is ${minElArr(arr)} has index ${arr.indexOf(minElArr(arr))}`);

// Знайти максимальний елемент масиву та його порядковий номер.
function maxElArr(arr) {
  let maxEl = 0;

  arr.forEach(function (el) {
    maxEl = el > maxEl ? el : maxEl;
  });
  
  return maxEl;
}

console.log(`Max num in array is ${maxElArr(arr)} has index ${arr.indexOf(maxElArr(arr))}`);

// Визначити кількість негативних елементів.
function findNegativeEl(arr) {
  let element = 0;

  arr.filter(function (el) {
    if (el <= 0) {
      element++;
    }
  });

  return element;
}

console.log(findNegativeEl(arr));

// Знайти кількість непарних позитивних елементів.
function unpairPositiveEl() {
  let counter = 0;
  
  arr.forEach(function (el) {
    if (el >= 0 && el % 2 !== 0) {
      counter++;
    }
  });

  return counter;
}

console.log(unpairPositiveEl());

// Знайти кількість парних позитивних елементів.
function pairPositiveEl() {
  let counter = 0;
  
  arr.forEach(function (el) {
    if (el >= 0 && el % 2 === 0) {
      counter++;
    }
  });

  return counter;
}

console.log(pairPositiveEl());


// Знайти суму парних позитивних елементів.
function sumPairsPositiveEl() {
  let sum = 0;

  arr.forEach(function (el) {
    if (el >= 0 && el % 2 === 0) {
      sum += el;
    }
  });

  return sum;
}

console.log(sumPairsPositiveEl());

// Знайти суму непарних позитивних елементів.
function sumUnpairsPositiveEl() {
  let sum = 0;

  arr.forEach(function (el) {
    if (el >= 0 && el % 2 !== 0) {
      sum += el;
    }
  });

  return sum;
}

console.log(sumUnpairsPositiveEl());

// Знайти добуток позитивних елементів.
function multElements(arr) {
  let sum = 1;
  arr.forEach(function (el) {
    if (el >= 0) {
      return sum *= el;
    }
  });

  return sum;
}

console.log(multElements(arr));

// Знайти найбільший серед елементів масиву, остальні обнулити.
function nullifyAllExceptMaxEl(arr) {
  let maxEl = 0;

  arr.forEach(function (el) {
    maxEl = el > maxEl ? el : maxEl;
  });

  const newArr = arr.map(function (el, i) {
    return arr[i] === maxEl ? maxEl : 0;
  });

  return newArr;
}

console.log(nullifyAllExceptMaxEl(arr));

console.log(arr);